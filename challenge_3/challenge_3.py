"""
Difficulty: medium
"""

# pip install all of the packages that are used
# if not yet installed
from bs4 import BeautifulSoup
import requests
import csv
from datetime import datetime

URL = 'https://www.camden.nsw.gov.au/council/council-meetings/2019-business-papers-and-minutes/'
# this is a header to be used to access website
# to provide information about the request context
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4"
                  "147.125 Safari/537.36",
    "Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8"
}


def pdf_sorter(file):
    """
    Add the corresponding files into matching dates
    in the existing dictionary
    """
    for pdf_item in file:
        if "agenda" in pdf_item.lower():
            date_format = f"{pdf_item.split('-')[3]}-{pdf_item.split('-')[4]}-2019"
            for item in meetings_info:
                if date_format in item.keys():
                    item["business_papers"] = pdf_item
        elif "attach" in pdf_item.lower():
            date_format = f"{pdf_item.split('-')[3]}-{pdf_item.split('-')[4]}-2019"
            for item in meetings_info:
                if date_format in item.keys():
                    item["attachments_links"] = pdf_item
        else:
            date_format = f"{pdf_item.split('Minutes-')[1].split('-')[0]}-{pdf_item.split('Minutes-')[1].split('-')[1]}-2019"
            for item in meetings_info:
                if date_format in item.keys():
                    item["minutes"] = pdf_item


# pull web data on the specified URL
response = requests.get(URL, headers=HEADERS)
data = response.text
# prints the web data to check for successful pull
# print(data)

soup = BeautifulSoup(data, "html.parser")
# select all the h2 tags of the table which contains the dates
meetings_dates_info = soup.select("div.col-md-9.content > h2")

meetings_info = []

for date in meetings_dates_info:
    # cleans the dates to be converted
    # append clean dates into a dictionary
    value = datetime.strptime(date.text.replace("\xa0", " ").lstrip(), '%d %B  %Y').strftime('%Y-%m-%d')
    key = datetime.strptime(date.text.replace("\xa0", " ").lstrip(), '%d %B  %Y').strftime('%d-%b-%Y')
    meetings_info.append({
        f"{key}": value,
        "meeting_date": value
    })

# get all the pdf in the table
meetings_pdf_info = soup.select("div.col-md-9.content > p > a")

business_paper_links = []
attachments_links = []
minutes_links = []

for pdf in meetings_pdf_info:
    # constant to be added to complete the link
    link = "https://www.camden.nsw.gov.au"
    # filter the pdf into their corresponding types (i.e. Business Paper, Attachments, Minutes)
    # append the filter pdf into a list
    if pdf.text.rstrip() == "Business Paper":
        business_paper_links.append(f"{link}{pdf.get('href')}")
    elif pdf.text.rstrip() == "Attachments":
        attachments_links.append(f"{link}{pdf.get('href')}")
    elif pdf.text.rstrip() == "Minutes":
        minutes_links.append(f"{link}{pdf.get('href')}")
    else:
        continue
# executes the function pdf sorter to compile all data into a dictionary
pdf_sorter(business_paper_links)
pdf_sorter(attachments_links)
pdf_sorter(minutes_links)

# create a csv file to be written on
with open('challenge_3_output.csv', 'w', newline='') as f:
    # headers for the csv file
    fieldnames = ['meeting_date', 'business_papers', 'attachments_links', 'minutes']
    meetings_csv = csv.DictWriter(f, fieldnames=fieldnames)

    meetings_csv.writeheader()
    # store data of agendas into a csv file
    for meetings in meetings_info:
        try:
            meetings_csv.writerow({
                'meeting_date': meetings['meeting_date'],
                'business_papers': meetings['business_papers'],
                'attachments_links': meetings['attachments_links'],
                'minutes': meetings['minutes'],
            })
        except KeyError:
            continue
