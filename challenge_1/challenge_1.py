"""
Difficulty: easy
"""

# from selenium import webdriver
# I could have use selenium and google forms to create a csv file
# But since it isn't necessary to accomplish the task I opt out in using it
# pip install all of the packages that are used
# if not yet installed
from bs4 import BeautifulSoup
import csv
import requests

URL = 'https://www.emservices.com.sg/tenders/'

# pull web data on the specified URL
response = requests.get(URL)
data = response.text
# prints the web data to check for successful pull
# print(data)

soup = BeautifulSoup(data, "html.parser")
# selects the table that comprises all the tenders in the website
tenders = soup.select("#tnTable1 > tbody > tr")

# a list of dictionary info of the tenders
tenders_info = []

for tender in tenders:
    # Get the link for the specific tender
    link = tender.a["href"]
    # Store data of the specific tender
    info = [link]
    # Access data from the tender table row and temporary store it in a list
    for data in tender:
        info.append(data.text)
    # Create a hash table to store the data required
    tenders_info.append({
        "advert_date": info[1],
        "closing_date": info[2],
        "client": info[3],
        "description": info[4],
        "eligibility": info[5],
        "link": info[0]
    })

# create a csv file to be written on
with open('challenge_1_output.csv', 'w', newline='') as f:
    # headers for the csv file
    fieldnames = ['advert_date', 'closing_date', 'client', 'description', 'eligibility', 'link']
    tenders_csv = csv.DictWriter(f, fieldnames=fieldnames)

    tenders_csv.writeheader()
    # store data of tenders into a csv file
    for tender in tenders_info:
        tenders_csv.writerow({
            'advert_date': tender['advert_date'],
            'closing_date': tender['closing_date'],
            'client': tender['client'],
            'description': tender['description'],
            'eligibility': tender['eligibility'],
            'link': tender['link'],
        })
