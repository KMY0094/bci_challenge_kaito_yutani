"""
Difficulty: medium
"""

# pip install all of the packages that are used
# if not yet installed
from bs4 import BeautifulSoup
import requests
import csv
from datetime import datetime

URL = 'https://businesspapers.parracity.nsw.gov.au/'

# pull web data on the specified URL
response = requests.get(URL)
data = response.text
# prints the web data to check for successful pull
# print(data)

soup = BeautifulSoup(data, "html.parser")
# selects the table that comprises all the agendas in the website
agendas = soup.select("tbody > tr")

agendas_info = []

for agenda in agendas:
    # select the class for the date and time in the table
    date_time_data = agenda.select_one(".bpsGridDate").text
    date = (date_time_data.split('2021')[0] + '2021').split(' ')
    time = date_time_data.split('2021')[1].upper()
    # convert the format for date and time
    date_str = f"{date[1]} {date[0]} {date[2]}"
    date_converted = datetime.strptime(date_str, '%b %d %Y').strftime('%Y-%m-%d')
    # concat the date and time together
    date_time = f"{date_converted} {time}"

    # select meeting description
    meeting_description = agenda.select_one(".bpsGridCommittee")
    # remove additional information
    remove_span = meeting_description.find('span').extract()
    meeting_description = meeting_description.text

    try:
        # select the href from the anchor tag for the agenda html link
        agenda_html_link = agenda.select_one(".bpsGridAgenda > .bpsGridHTMLLink").get('href')
    except AttributeError:
        agenda_html_link = ""

    # error handling in cases where there is no link
    try:
        # select the href from the anchor tag for the agenda pdf link
        agenda_pdf_link = agenda.select_one(".bpsGridAgenda > .bpsGridPDFLink").get('href')
    except AttributeError:
        agenda_pdf_link = ""

    # error handling in cases where there is no link
    try:
        # select the href from the anchor tag for the agenda minutes link
        minutes_html_link = agenda.select_one(".bpsGridMinutes > .bpsGridHTMLLink").get('href')
    except AttributeError:
        minutes_html_link = ""

    agendas_info.append({
        "meeting_date": date_time,
        "meeting_description": meeting_description,
        "agenda_html_link": agenda_html_link,
        "agenda_pdf_link": agenda_pdf_link,
        "minutes_html_link": minutes_html_link,
    })

# create a csv file to be written on
with open('challenge_2_output.csv', 'w', newline='') as f:
    # headers for the csv file
    fieldnames = ['meeting_date', 'meeting_description', 'agenda_html_link', 'agenda_pdf_link', 'minutes_html_link']
    agendas_csv = csv.DictWriter(f, fieldnames=fieldnames)

    agendas_csv.writeheader()
    # store data of agendas into a csv file
    for agenda in agendas_info:
        agendas_csv.writerow({
            'meeting_date': agenda['meeting_date'],
            'meeting_description': agenda['meeting_description'],
            'agenda_html_link': agenda['agenda_html_link'],
            'agenda_pdf_link': agenda['agenda_pdf_link'],
            'minutes_html_link': agenda['minutes_html_link'],
        })
